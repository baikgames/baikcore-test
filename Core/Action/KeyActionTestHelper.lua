-- Load Modules
local Baik = _G.Baik
local Class = Baik.Base.Class
local IRepository = Baik.Core.Interface.IRepository

local BaikTest = _G.BaikTest
local UnitTest = BaikTest.UnitTest

-- Create Module
local KeyActionTestHelper = Class()

-- Constants
local _DEFAULT = "pizza"
local _TESTS = {
    {
        name = "Get",
        action = function(action, value) action:Get() end,
        value = nil,
    },
    {
        name = "Set",
        action = function(action, value) action:Set(value) end,
        value = nil,
    },
    {
        name = "Observe",
        action = function(action, value) action:Observe(value) end,
        value = function(value) end
    },
    {
        name = "Unobserve",
        action = function(action, value) action:Unobserve(value) end,
        value = function(value) end
    }
}

-- Private Class
local Repo = Class(IRepository)

function Repo:Setup(value)
    self._data = {}
    self._default = value
end

function Repo:Default(key, value)
    self._data[key] = value
end

function Repo:Get(key)
    self._data[key] = self._default
    return self._default
end

function Repo:_Set(key, value)
    self._data[key] = value
end

function Repo:Observe(key, observer)
    self._data[key] = observer
end

function Repo:Unobserve(key, observer)
    self._data[key] = observer
end

-- Private Methods
local function _TestInit(action, key, expected)
    -- Setup
    local repo = Class(Repo)
    repo:Setup()

    -- Run Test
    action:Init(repo)

    -- Check Result
    local result = repo._data[key]
    return UnitTest.RunTest("Init", expected, result)
end

local function _Test(name, action, test_action, key, expected)
    -- Setup
    local repo = Class(Repo)
    repo:Setup(expected)

    -- Run Test
    action:Init(repo)
    test_action(action, expected)

    -- Check Result
    local result = repo._data[key]
    return UnitTest.RunTest(name, expected, result)
end

-- Public API
function KeyActionTestHelper.Test(action, key, default)
    local result = true

    result = _TestInit(action, key, default) and result

    for _, test in ipairs(_TESTS) do
        result = _Test(test.name,
                       action,
                       test.action,
                       key,
                       test.value or default) and result
    end

    return result
end

-- Export Module
BaikTest.KeyActionTestHelper = KeyActionTestHelper
