-- Load Modules
local Baik = _G.Baik
local Class = Baik.Base.Class
local DebugAction = Baik.Core.Action.DebugAction

local BaikTest = _G.BaikTest
local UnitTest = BaikTest.UnitTest
local KeyActionTestHelper = BaikTest.KeyActionTestHelper

-- Create Module
local DebugActionTest = UnitTest.New("DebugAction")

-- Override API
function DebugActionTest:Run()
    return KeyActionTestHelper.Test(DebugAction,
                                    DebugAction._key,
                                    DebugAction._default)
end
