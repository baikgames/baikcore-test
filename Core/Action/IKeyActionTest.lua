-- Load Modules
local Baik = _G.Baik
local Class = Baik.Base.Class
local IKeyAction = Baik.Core.Action.IKeyAction

local BaikTest = _G.BaikTest
local UnitTest = BaikTest.UnitTest
local KeyActionTestHelper = BaikTest.KeyActionTestHelper

-- Create Module
local IKeyActionTest = UnitTest.New("IKeyAction")

-- Override API
function IKeyActionTest:Run()
    local key = "key"
    local default = "default"
    local action = IKeyAction.New(key, default)
    function action:_FromHash(value)
        return value
    end

    return KeyActionTestHelper.Test(action, key, default)
end
