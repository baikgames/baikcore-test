-- Load Modules
local Baik = _G.Baik
local Class = Baik.Base.Class
local Table = Baik.Base.Table
local IRepository = Baik.Core.Interface.IRepository

local BaikTest = _G.BaikTest
local UnitTest = BaikTest.UnitTest

-- Create Module
local IRepositoryTest = UnitTest.New("IRepository")

-- Constants
local _KEY = "test_key"
local _DEFAULT = "test_value"
local _TESTS = {
    {
        name = "Default",
        action = function(repo, key, value) repo:Default(key, value) end,
        value = _DEFAULT
    },
    {
        name = "Enable",
        action = function(repo, key, value) repo:Enable(value) end,
        value = _DEFAULT
    },
    {
        name = "Get",
        action = function(repo, key, value) repo:Get(key) end,
        value = _DEFAULT
    },
    {
        name = "_Set",
        action = function(repo, key, value) repo:Set(key, value) end,
        value = _DEFAULT
    },
}

-- Private Class
local TestRepo = Class(IRepository)

function TestRepo:Enable(value)
    self.data[_KEY] = value
end

function TestRepo:Default(key, value)
    self.data[key] = value
end

function TestRepo:Get(key)
    self.data[key] = _DEFAULT
end

function TestRepo:_Set(key, value)
    self.data[key] = value
end

function TestRepo:Setup()
    self.data = {}
end

-- Private Methods
local function _Test(name, action, expected)
    -- Setup
    local repo = Class(TestRepo)
    repo:Setup()

    -- Run Test
    action(repo, _KEY, expected)

    -- Check Result
    local result = repo.data[_KEY]
    return UnitTest.RunTest(name, expected, result)
end

-- Override Methods
function IRepositoryTest:Run()
    local result = true

    Table.ForEach(_TESTS, function(test)
        result = _Test(test.name, test.action, test.value) and result
    end)

    return result
end
