-- Load Modules
local Baik = _G.Baik
local DebugStruct = Baik.Core.Struct.DebugStruct

local BaikTest = _G.BaikTest
local UnitTest = BaikTest.UnitTest

-- Create Module
local DebugStructTest = UnitTest.New("DebugStruct")

-- Private Variables
local _NAMES = DebugStruct.NAMES
local _LEVEL_START = DebugStruct._TEST_LEVEL_START
local _LEVEL_END = DebugStruct._TEST_LEVEL_END

local _DEBUGS = DebugStruct._TEST_LEVELS

-- Private Methods
local function _TestLevel(level, name)
    if level < _LEVEL_START and level > _LEVEL_END then
        local msg = string.format("Level_%s", name)
        UnitTest.PrintError(msg)
        return false
    end

    return true
end

local function _TestName(level, expected)
    local result = DebugStruct.ToString(level)
    if result ~= expected then
        local test_name = string.format("Name_%s", result)
        UnitTest.PrintTestError(test_name, expected, result)
        return false
    end

    return true
end

-- Override Method
function DebugStructTest:Run()
    local result = true
    for level, name in pairs(_NAMES) do
        if not _TestLevel(level, name) then
            result = false
        end

        if not _TestName(level, name) then
            result = false
        end
    end

    return result
end
