-- Load Module
local _G = _G
local BaikTest = LibStub("AceAddon-3.0"):NewAddon("BaikTest", "AceConsole-3.0")
_G.BaikTest = BaikTest

-- Ace Callbacks
function BaikTest:OnInitialize()
end

function BaikTest:OnEnable()
    BaikTest.TestRunner:Run()
end

function BaikTest:OnDisable()
end
