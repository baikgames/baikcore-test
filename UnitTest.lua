-- Load Modules
local Baik = _G.Baik
local BaikTest = _G.BaikTest
local Class = Baik.Base.Class
local NotImplementedError = Baik.Error.NotImplementedError
local TestRunner = BaikTest.TestRunner

-- Create Module
local UnitTest = Class()
UnitTest.__index = UnitTest

-- Class Method
function UnitTest.New(name)
    local obj = Class(UnitTest)

    obj._name = name
    TestRunner:Add(obj)

    return obj
end

-- Public API
function UnitTest.PrintTestError(name, expected, result)
    BaikTest:Printf("  - %s", name)
    BaikTest:Printf("      %s", expected or "nil")
    BaikTest:Printf("      %s", result or "nil")
end

function UnitTest.PrintError(msg)
    BaikTest:Printf("  - %s", msg)
end

function UnitTest.RunTest(name, expected, result)
    if result ~= expected then
        UnitTest.PrintTestError(name, expected, result)
        return false
    end

    return true
end

function UnitTest:Run()
    NotImplementedError()
end

function UnitTest:GetName()
    return self._name
end

-- Export Module
BaikTest.UnitTest = UnitTest
