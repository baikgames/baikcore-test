-- Load Module
local Baik = _G.Baik
local BaikTest = _G.BaikTest

-- Create Module
local TestRunner = Baik.Base.Class()

-- Private Variables
local _tests = {}

-- Private Methods
local function _RunTest(test)
    local test_name = test:GetName()
    BaikTest:Printf("  %s", test_name)
    local result = test:Run()
    if result ~= true then
        BaikTest:Print("FAIL")
    end

    return result
end

-- Public API
function TestRunner:Run()
    BaikTest:Print("Running Tests")
    local total_tests = #_tests
    local total_passed = 0
    for idx = 1, total_tests, 1 do
        local test = _tests[idx]
        if _RunTest(test) then
            total_passed = total_passed + 1
        end
    end

    BaikTest:Printf("Passed %d/%d", total_passed, total_tests)
end

function TestRunner:Add(test)
    table.insert(_tests, test)
end

-- Export Module
BaikTest.TestRunner = TestRunner
