-- Load Modules
local Baik = _G.Baik
local BaikTest = _G.BaikTest
local Class = Baik.Base.Class
local Type = Baik.Base.Type

-- Create Module
local TypeTest = BaikTest.UnitTest.New("Type")

-- Member Variable
local _PARENT_A = Class()
local _CHILD_A = Class(_PARENT_A)
local _PARENT_B = Class()

local _CATEGORY_BOOL = "bool"
local _CATEGORY_FUN = "function"
local _CATEGORY_NUM = "number"
local _CATEGORY_STR = "string"
local _CATEGORY_TBL = "table"
local _CATEGORY_NIL = "nil"

local _TYPES = {
    [_CATEGORY_BOOL] = { func = Type.IsBoolean,  name = "IsBoolean" },
    [_CATEGORY_FUN]  = { func = Type.IsFunction, name = "IsFunction" },
    [_CATEGORY_NUM]  = { func = Type.IsNumber,   name = "IsNumber" },
    [_CATEGORY_STR]  = { func = Type.IsString,   name = "IsString" },
    [_CATEGORY_TBL]  = { func = Type.IsTable,    name = "IsTable" },
}

local _TESTS = {
    { category = _CATEGORY_BOOL, val = true,           msg = "Bool_True"},
    { category = _CATEGORY_BOOL, val = false,          msg = "Bool_False"},
    { category = _CATEGORY_FUN,  val = function() end, msg = "Function"},
    { category = _CATEGORY_NUM,  val = 1,              msg = "Number_Int"},
    { category = _CATEGORY_NUM,  val = 1.0,            msg = "Number_Float"},
    { category = _CATEGORY_STR,  val = "fail",         msg = "String"},
    { category = _CATEGORY_TBL,  val = {},             msg = "Table"},
    { category = _CATEGORY_NIL,  val = nil,            msg = "Nil"},
}

local _CLASS_TESTS = {
    { parent = _CHILD_A,  expected = true,  msg = "Same" },
    { parent = _PARENT_A, expected = true,  msg = "Child" },
    { parent = _PARENT_B, expected = false, msg = "Not Child" },
}

-- Private Methods
local function _TestAll(category, test_func, name)
    local result = true
    for _, test in pairs(_TESTS) do
        local expected = test.category == category
        local test_result = test_func(test.val)
        if test_result ~= expected then
            local test_name = string.format("%s_%s", name, test.msg)
            UnitTest.PrintTestError(test_name, expected, test_result)
            result = false
        end
    end

    return result
end

local function _TestChild(test)
    local test_result = Type.IsChild(_CHILD_A, test.parent)
    if test_result ~= test.expected then
        local test_name = string.format("%s_%s", "IsChild", test.msg)
        UnitTest.PrintTestError(test_name, test.expected, test_result)
        return false
    end

    return true
end

-- Override Method
function TypeTest:Run()
    local result = true
    for category, type_data in pairs(_TYPES) do
        result = _TestAll(category, type_data.func, type_data.name) == true
                    and result
    end

    for _, test in pairs(_CLASS_TESTS) do
        result = _TestChild(test) == true
                    and result
    end

    return result
end
