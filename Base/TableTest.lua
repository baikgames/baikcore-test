-- Load Modules
local Baik = _G.Baik
local BaikTest = _G.BaikTest
local Table = Baik.Base.Table
local UnitTest = BaikTest.UnitTest

-- Create Module
local TableTest = UnitTest.New("Table")

-- Private Methods
local function _TestRemoveValue()
    -- Initialize test object
    local obj = {}
    local value = "dog"
    table.insert(obj, value)

    -- Remove value
    Table.RemoveValue(obj, value)
    if #obj > 0 then
        UnitTest.PrintTestError("TestRemoveValue", 0, #obj)
        return false
    end

    return true
end

local function _TestContainsValue()
    local obj = {}
    local value = "dog"

    -- Check empty table
    local result = Table.ContainsValue(obj, value)
    if result ~= false then
        UnitTest.PrintTestError("TestContainsValue_Empty", false, result)
        return false
    end

    -- Check inserted table
    table.insert(obj, value)
    result = Table.ContainsValue(obj, value)
    if result ~= true then
        UnitTest.PrintTestError("TestContainsValue_HasValue", true, result)
        return false
    end

    return true
end

local function _TestForEach()
    -- Initialize test object
    local obj = {}
    local max = 5
    for idx=1, 5, 1 do
        table.insert(obj, idx)
    end

    -- Fill new object with ForEach
    local newObj = {}
    Table.ForEach(obj, function(value)
        newObj[value] = true
    end)

    -- Check values
    for idx=#obj, 1, -1 do
        local value = obj[idx]
        local result = newObj[value]
        if result ~= true then
            local test_name = string.format("TestForEach_%s", value)
            UnitTest.PrintTestError(test_name, true, result)
            return false
        end
    end

    return true
end

-- Override Methods
function TableTest:Run()
    local result = true

    result = _TestRemoveValue() and result
    result = _TestContainsValue() and result
    result = _TestForEach() and result

    return result
end
